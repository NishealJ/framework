const { spawn } = require('child_process')
const path = require('path')
const express = require('express')
const app = express()

app.use(express.json({ limit: '500kb' }))
app.post('/format/u', ({ body, query: { base } }, res) => {
    const conllU = spawn('python3', [
        path.resolve(__dirname, 'conll-u.py')
    ])

    let stdout = '';
    let stderr = '';
    conllU.stdout.on('data', data => { stdout += data.toString() });
    conllU.stderr.on('data', data => { stderr += data.toString() });

    conllU.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send('Processing CDLI-CoNLL failed')
        } else {
            res.send(stdout)
        }
    })

    conllU.stdin.write(body.annotation)
    conllU.stdin.end()
})

module.exports = {
    app,
    message: `Running CoNLL-U...`
}
