<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class AdvancedsearchController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'ElasticSearch'
        $this->loadComponent('ElasticSearch');

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * beforeFilter method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->session = $this->getRequest()->getSession();

        // Get search Settings for Session
        $this->searchSettings = $this->session->read('searchSettings');

        // Searchable parameters on view
        $this->searchableFields = [
            "pdesignation", "authors", "editors", "year",
            "title", "ptype", "publisher", "series",
            "atype", "materials", "collection", "provenience",
            "archive", "period", "acomments", "translation",
            "transliteration", "icomments", "structure", "genres",
            "languages", "adesignation", "museum_no", "accession_no",
            "id", "seal_no", "composite_no"
        ];

        // Search Result View Settings
        $this->settings = [
            'LayoutType' => 1,
            'Page' => 1,
            'PageSize' => $this->searchSettings['PageSize'],
            'lastPage' => 0,
            'canViewPrivateArtifacts' => $this->GeneralFunctions->checkIfRolesExists([1, 4]) == 1 ? 1 : 0,
            'canViewPrivateInscriptions' => $this->GeneralFunctions->checkIfRolesExists([1, 5]) == 1 ? 1 : 0,
            'sortBy' => 'relevance',
            'filter_dirty' => 0
        ];

        $this->filters = [
            'collection' => [],
            'period' => [],
            'provenience' => [],
            'atype' => [],
            'materials' => [],
            'authors' => [],
            'year' => []
        ];

        // To store whole array result with key as searchId (as timestamp) in session vaiable
        // Max searchId to be store = 4
        if (is_null($this->session->read('resultsStored'))) {
            $this->session->write('resultsStored', []);
            $this->resultsStored = [];
        } else {
            $this->resultsStored = $this->session->read('resultsStored');
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        $queryData = $this->getDataFromRequest($this->getRequest()->getData());

        if (!empty($queryData)) {
            $searchId = time();

            $this->getSearchResults($queryData, $searchId);

            return $this->redirect([
                'action' => 'view',
                $searchId,
                '?' => $queryData
            ]);
        }
    }

    /**
     * filter method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function filter($searchId)
    {
        // Filter submitted through form
        $filterData = $this->getRequest()->getData();

        $searchIdArray = explode('-', $searchId);

        if ($searchIdArray[0] === 'reset') {
            $searchId = $searchIdArray[1];
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $toBeReset = [];

            foreach ($currentFilters as $filter => $values) {
                $toBeReset[$filter] = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));
            }

            foreach ($toBeReset as $filter => $values) {
                foreach ($values as $value) {
                    $currentFilters[$filter][$value] = 0;
                }
            }
        } else {
            // Get filters stored in session
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $currentAppliedFilters = [];

            // Extract filters values which are set
            foreach ($currentFilters as $filter => $values) {
                // Check if filter values are selected or not
                if (!empty($values)) {
                    $selectedValues = array_keys(array_filter(
                        $values,
                        function ($value) {
                            return $value;
                        }
                    ));

                    // If empty check if the $filter exists in $filterData
                    if (!empty($selectedValues)) {
                        $currentAppliedFilters[$filter] = $selectedValues;
                    } else {
                        if (array_key_exists($filter, $filterData)) {
                            $currentAppliedFilters[$filter] = [];
                        }
                    }
                } else {
                    // If empty check if the $filter exists in $filterData
                    if (array_key_exists($filter, $filterData)) {
                        $currentAppliedFilters[$filter] = [];
                    }
                }
            }

            // Check if filters are updated
            $checkIfFilterChanged = 0;

            // Calculate difference between requested and store filters
            foreach ($currentAppliedFilters as $filter => $value) {
                $newValue = $filterData[$filter];
                $newValue = $newValue == '' ? [] : $newValue;

                $arrayDiffRemoved = array_diff($value, $newValue);
                $arrayDiffAdded = array_diff($newValue, $value);

                // If there is change in requested filter applied
                if (!empty($arrayDiffAdded) || !empty($arrayDiffRemoved)) {
                    $checkIfFilterChanged = 1;

                    // If unselected
                    foreach (array_diff($value, $newValue) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 0;
                    }

                    // If selected
                    foreach (array_diff($newValue, $value) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 1;
                    }
                }
            }

            // If there is no filter change then redirect to previous page
            if (!$checkIfFilterChanged) {
                $this->redirect($this->referer());
            }
        }

        // Set filter_dirty
        $this->session->write('resultsStored.'.$searchId.'.settings.filter_dirty', 1);

        // Set new filter
        $this->session->write('resultsStored.'.$searchId.'.filters', $currentFilters);

        $queryData = [];

        foreach ($this->getRequest()->getQueryParams() as $param => $values) {
            $status = in_array($param, $this->searchableFields) || $param == 'LayoutType';
            if ($status) {
                $queryData = array_merge($queryData, [$param => $values]);
            }
        }

        $params = [];
        $params = array_merge($params, $queryData);

        return $this->redirect([
            'action' => 'view',
            $searchId,
            '?' => $params
        ]);
    }

    /**
     * view method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function view($searchId)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        if (substr($searchId, 0, 6) === 'filter') {
            return $this->setaction('filter', substr($searchId, 7));
        } else {
            $queryData = $this->getDataFromRequest($this->getRequest()->getQueryParams());

            $resultSet = empty($this->resultsStored) ? -1 : (array_key_exists($searchId, $this->resultsStored) ? $this->resultsStored[$searchId]['resultSet'] : null);

            // To check if redirected from filter function
            $filterDirtyStatus = empty($this->resultsStored) ? 0 : $this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty');

            if ($filterDirtyStatus) {
                $this->settings['Page'] = 1;
            }

            $requestedResultSet = (int)floor(($this->settings['Page'] - 1) * $this->settings['PageSize']/ 10000);

            // To check if requested page is within the resultSet.
            $outOfBoundPage = 0;

            if (!is_null($resultSet) && $requestedResultSet != $resultSet) {
                $outOfBoundPage = 1;
            }

            if (!array_key_exists($searchId, $this->resultsStored) || $outOfBoundPage || $filterDirtyStatus) {
                $this->getSearchResults($queryData, $searchId);
            }

            if ($requestedResultSet > 0) {
                $offset = ($this->settings['Page'] - 1) * $this->settings['PageSize'] - $requestedResultSet * 10000;
            } else {
                $offset = ($this->settings['Page'] == 1) ? 0 : ($this->settings['Page'] - 1) * $this->settings['PageSize'];
            }

            $result = array_slice($this->resultsStored[$searchId]['result'], $offset, $this->settings['PageSize'], true);

            $this->settings['lastPage'] = (int)ceil($this->resultsStored[$searchId]['totalHits']/$this->settings['PageSize']);

            foreach ($result as $artifactID => $values) {
                $result[$artifactID] = array_merge(
                    $result[$artifactID],
                    $this->ElasticSearch->getPublicationWithArtifactId($artifactID)
                );

                $result[$artifactID]['inscription'] = $this->ElasticSearch->getInscriptionWithArtifactId($artifactID, $this->settings['canViewPrivateInscriptions']);
            }

            $commodityVizIds = array();
            foreach ($this->resultsStored[$searchId]['result'] as $artifactID => $values) {
                if ($values['languages']['languages'] == 'Sumerian') {
                    array_push($commodityVizIds, $artifactID);
                }
            }

            $this->set([
                'searchId' => $searchId,
                'result' => $result,
                'LayoutType' => $this->settings['LayoutType'],
                'PageSize' => $this->settings['PageSize'],
                'Page' => $this->settings['Page'],
                'lastPage' => $this->settings['lastPage'],
                'searchSettings' => $this->searchSettings,
                'filters' => $this->resultsStored[$searchId]['filters'],
                'searchableFields' => $this->searchableFields,
                'commodityVizIds' => $commodityVizIds,
            ]);
        }
    }

    /**
     * getDataFromRequest method
     *
     *  It returns array of required Key=>Value Pair.
     *
     * @param
     * param : Type of Request i.e. data(URL params) or query(form POST) parameters.
     *
     * @return Array of Key=>Value pair.
     */
    public function getDataFromRequest($requestType)
    {
        $queryData = [];

        foreach ($requestType as $key => $value) {
            if ($value !== '') {
                if (array_key_exists($key, $this->settings)) {
                    $this->settings[$key] = $value;
                } elseif (in_array($key, $this->searchableFields)) {
                    $queryData[$key] = trim($value, '" || \'');
                }
            }
        }

        return $queryData;
    }

    /**
     * getFilterDataFromSession method
     *
     * Get Filters stored in Session for specific searchId.
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return Array of Filters
     */
    public function getFilterDataFromSession($searchId)
    {
        $currentFilters = $this->getRequest()->getSession()->read('resultsStored.'.$searchId.'.filters');

        $currentFilters = is_null($currentFilters) ? [] : $currentFilters;

        $setFilters = [];

        foreach ($currentFilters as $filter => $values) {
            if (!empty($values)) {
                $selectedValues = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));

                if (!empty($selectedValues)) {
                    $setFilters[$filter] = $selectedValues;
                }
            }
        }

        return $setFilters;
    }

    /**
     * getSearchResults method
     *
     * @param
     * queryData : Array of [Key => Value] where Key is searchCategory and value is value of searchCategory.
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function getSearchResults($queryData, $searchId)
    {
        // If there are 4 search results already stored in session.
        if (sizeof($this->resultsStored) == 4) {
            $firstInsertedKey = min(array_keys($this->resultsStored));
            if ($searchId != $firstInsertedKey) {
                unset($this->resultsStored[$firstInsertedKey]);
            }
        }

        $filterData = $this->getFilterDataFromSession($searchId);

        // Retrieve results and updated settings from ElasticSearch Components
        $resultFromESComponent = $this->ElasticSearch->searchAdvancedArtifacts($queryData, $this->settings, $filterData);

        if ($this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty') || !empty($filterData)) {
            $resultFromESComponent['filters'] = $this->session->read('resultsStored.'.$searchId.'.filters');
        }

        $resultFromESComponent['settings'] = $this->settings;

        $this->resultsStored[$searchId] = $resultFromESComponent;

        // Store in Session variables
        $this->session->write('resultsStored', $this->resultsStored);
    }
}
