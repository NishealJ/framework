<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Rulers Controller
 *
 * @property \App\Model\Table\RulersTable $Rulers
 *
 * @method \App\Model\Entity\Ruler[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RulersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $ruler = $this->Rulers->newEntity();
        if ($this->getRequest()->is('post')) {
            $ruler = $this->Rulers->patchEntity($ruler, $this->getRequest()->getData());
            if ($this->Rulers->save($ruler)) {
                $this->Flash->success(__('The ruler has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The ruler could not be saved. Please, try again.'));
        }
        $periods = $this->Rulers->Periods->find('list', ['limit' => 200]);
        $dynasties = $this->Rulers->Dynasties->find('list', ['limit' => 200]);
        $this->set(compact('ruler', 'periods', 'dynasties'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $ruler = $this->Rulers->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $ruler = $this->Rulers->patchEntity($ruler, $this->getRequest()->getData());
            if ($this->Rulers->save($ruler)) {
                $this->Flash->success(__('The ruler has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The ruler could not be saved. Please, try again.'));
        }
        $periods = $this->Rulers->Periods->find('list', ['limit' => 200]);
        $dynasties = $this->Rulers->Dynasties->find('list', ['limit' => 200]);
        $this->set(compact('ruler', 'periods', 'dynasties'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $ruler = $this->Rulers->get($id);
        if ($this->Rulers->delete($ruler)) {
            $this->Flash->success(__('The ruler has been deleted.'));
        } else {
            $this->Flash->error(__('The ruler could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
