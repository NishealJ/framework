<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactTypes Controller
 *
 * @property \App\Model\Table\ArtifactTypesTable $ArtifactTypes
 *
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactTypesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['ParentArtifactTypes']
        ];
        $artifactTypes = $this->paginate($this->ArtifactTypes);

        $this->set(compact('artifactTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => ['ParentArtifactTypes', 'ChildArtifactTypes', 'Artifacts']
        ]);

        $this->set('artifactType', $artifactType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifactType = $this->ArtifactTypes->newEntity();
        if ($this->getRequest()->is('post')) {
            $artifactType = $this->ArtifactTypes->patchEntity($artifactType, $this->getRequest()->getData());
            if ($this->ArtifactTypes->save($artifactType)) {
                $this->Flash->success(__('The artifact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact type could not be saved. Please, try again.'));
        }
        $parentArtifactTypes = $this->ArtifactTypes->ParentArtifactTypes->find('list', ['limit' => 200]);
        $this->set(compact('artifactType', 'parentArtifactTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $artifactType = $this->ArtifactTypes->patchEntity($artifactType, $this->getRequest()->getData());
            if ($this->ArtifactTypes->save($artifactType)) {
                $this->Flash->success(__('The artifact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact type could not be saved. Please, try again.'));
        }
        $parentArtifactTypes = $this->ArtifactTypes->ParentArtifactTypes->find('list', ['limit' => 200]);
        $this->set(compact('artifactType', 'parentArtifactTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $artifactType = $this->ArtifactTypes->get($id);
        if ($this->ArtifactTypes->delete($artifactType)) {
            $this->Flash->success(__('The artifact type has been deleted.'));
        } else {
            $this->Flash->error(__('The artifact type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
