<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Staff Controller
 *
 * @property \App\Model\Table\StaffTable $Staff
 *
 * @method \App\Model\Entity\Staff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StaffController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Authors', 'StaffTypes']
        ];
        $staff = $this->paginate($this->Staff);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('staff', 'access_granted'));
    }

    /**
     * View method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $staff = $this->Staff->get($id, [
            'contain' => ['Authors', 'StaffTypes']
        ]);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('access_granted'));
        $this->set('staff', $staff);
    }
}
