<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Materials Controller
 *
 * @property \App\Model\Table\MaterialsTable $Materials
 *
 * @method \App\Model\Entity\Material[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentMaterials',  'ChildMaterials']
        ];
        $materials = $this->paginate($this->Materials);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('materials', 'access_granted'));
        $this->set('_serialize', 'materials');
    }

    /**
     * View method
     *
     * @param string|null $id Material id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $material = $this->Materials->get($id, [
            'contain' => ['ParentMaterials',  'ChildMaterials']
        ]);

        $artifacts = $this->loadModel('artifacts_materials');
        $count = $artifacts->find('list', ['conditions' => ['material_id' => $id]])->count();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('material', 'count', 'access_granted'));
        $this->set('_serialize', 'material');
    }
}
