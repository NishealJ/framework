<?php

 namespace App\Controller\Component;

 use Cake\Controller\Component;
 use Cake\ORM\TableRegistry;
 use Aws\S3\S3Client;
 use Aws\Exception\AwsException;
 use Cake\Core\Exception\Exception;
 use Cake\Core\Configure;

 require 'vendor/autoload.php';

//  date_default_timezone_set('America/Los_Angeles');

 class FileserviceComponent extends Component
 {
     private $s3 = null;
     private $activetime = '+10 minutes';

     public function initialize(array $config): void
     {
         parent::initialize($config);
         $this->s3 = S3Client::factory([
            'credentials' => [
               'key' => Configure::read('minioakey'),
               'secret' => Configure::read('minioskey')
            ],
            'version' => Configure::read('minioversion'),
            'region'  => Configure::read('minioregion'),
            'endpoint' => Configure::read('minioproxy'),
            'use_path_style_endpoint' => true,
        ]);
     }

     private function _save($file, $path, $bucket)
     {
         if (file_exists($path)) {
             try {
                 $result = $this->s3->putObject([
                    'Bucket' => $bucket,
                    'Key' => $file,
                    'SourceFile' => $path
                ]);
                 // $result = $this->s3->createBucket(array('Bucket' => 'mybucket'));
                // print_r($result);
             } catch (S3Exception $e) {
                 error_log($e->getMessage() . "\n");
             }
         } else {
             $this->log('Failed to put file into minio :: File not found at ' . $file, 'error');
         }
     }

     private function _getPresignedFile($filename, $bucket)
     {
         try {
             $request = $this->s3->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $filename
            ]);
             $uri = $this->s3->createPresignedRequest($request, $this->activetime);
             $url = $uri->getURI()->__toString();
             $url = str_replace('http://minio:9000', '/minio', $url);
             return $url;
         } catch (S3Exception $e) {
             $this->log('Failed to get file from minio :: File not found at ' . $filename . '\n' . $e->getMessage(), 'error');
         }
     }

     public function presignedsave($bucket, $name)
     {
         try {
             $key = uniqid() . '_' . $name;
             $this->log($key, 'debug');
             $request = $this->s3->getCommand('PutObject', [
                'Bucket' => $bucket,
                'Key' => $key
            ]);
             // $this->log($request->__toString(), 'debug');
             // echo $request;
             $uri = $this->s3->createPresignedRequest($request, $this->activetime);
             $url = $uri->getURI()->__toString();
             $url = str_replace('http://minio:9000', '/minio', $url);
             return array('url' => $url, 'key' => $key);
         } catch (S3Exception $e) {
             $this->log($e->getMessage(), 'error');
         }
     }



     public function checkFile($filename, $bucket)
     {
         return $this->s3->doesObjectExist($bucket, $filename);
     }


     public function save($user, $file, $path, $bucket)
     {
         return $this->_save($file, $path, $bucket);
     }


     public function getPresignedFile($filename, $bucket)
     {
         return $this->_getPresignedFile($filename, $bucket);
     }
 }
