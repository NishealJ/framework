<?php
namespace App\View;

use Cake\Event\EventManager;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\View\SerializedView;

class BibliographyView extends SerializedView
{
    use SerializeTrait;

    /**
     * @var array
     */
    protected static $defaultOptions = [
        'format' => 'text',
        'template' => 'chicago-author-date'
    ];

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize', '_options'];

    /**
     * @var string
     */
    protected $_responseType = 'bibliography';

    public function initialize(): void
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize): string
    {
        return $this->Scripts->formatReference(
            $this->_dataToSerialize($serialize),
            'bibliography',
            $this->_optionsToSerialize()
        );
    }

    /**
     * @param array $options
     * @return array
     */
    protected function _optionsToSerialize($options = [])
    {
        $options = $this->viewVars['_options'];
        return array_merge(self::$defaultOptions, $options);
    }
}
