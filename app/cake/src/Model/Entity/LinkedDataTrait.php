<?php
namespace App\Model\Entity;

use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

trait LinkedDataTrait
{
    public function getUri($id = null)
    {
        if (empty($id)) {
            $id = $this->id;
        }

        $table = TableRegistry::get($this->getSource());
        $key = Inflector::variable($table->getTable());

        // Following would be for routes like /artifacts_materials/1
        // which are not used, apparently.
        // $key = Inflector::underscore($table->getTable());

        return $key . '/' . $id;
    }

    protected static function getUris($array)
    {
        if (!is_array($array)) {
            return [];
        }

        return array_map(function ($entity) {
            return $entity->getUri();
        }, $array);
    }

    protected static function getEntity($entity, ...$args)
    {
        if (empty($entity)) {
            return null;
        }

        // return $entity->getCidocCrm(...$args);
        return ['@id' => $entity->getUri()];
    }

    protected static function getEntities($array, ...$args)
    {
        if (!is_array($array)) {
            return [];
        }

        return array_map(function ($entity) use ($args) {
            return self::getEntity($entity, ...$args);
        }, $array);
    }

    protected static function getIdentifier($value, $type)
    {
        if (empty($value)) {
            return null;
        }

        return [
            'rdfs:label' => $value,
            '@type' => 'cdli:identifier_' . $type
        ];
    }

    protected static function getDimension($value, $dimension, $unit)
    {
        if (empty($value)) {
            return null;
        }

        return [
            '@type' => 'cdli:dimension_' . $dimension,
            'crm:P90_has_value' => $value,
            'crm:P91_has_unit' => ['@id' => 'cdli:unit_' . $unit]
        ];
    }

    abstract public function getCidocCrm();
}
