<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EntryType Entity
 *
 * @property int $id
 * @property string|null $label
 * @property string|null $author
 * @property string|null $title
 * @property string|null $journal
 * @property string|null $year
 * @property string|null $volume
 * @property string|null $pages
 * @property string|null $number
 * @property string|null $month
 * @property string|null $eid
 * @property string|null $note
 * @property string|null $crossref
 * @property string|null $keyword
 * @property string|null $doi
 * @property string|null $url
 * @property string|null $file
 * @property string|null $citeseerurl
 * @property string|null $pdf
 * @property string|null $abstract
 * @property string|null $comment
 * @property string|null $owner
 * @property string|null $timestamp
 * @property string|null $review
 * @property string|null $search
 * @property string|null $publisher
 * @property string|null $editor
 * @property string|null $series
 * @property string|null $address
 * @property string|null $edition
 * @property string|null $howpublished
 * @property string|null $lastchecked
 * @property string|null $booktitle
 * @property string|null $organization
 * @property string|null $language
 * @property string|null $chapter
 * @property string|null $type
 * @property string|null $school
 * @property string|null $nationality
 * @property string|null $yearfiled
 * @property string|null $assignee
 * @property string|null $day
 * @property string|null $dayfiled
 * @property string|null $monthfiled
 * @property string|null $institution
 * @property string|null $revision
 *
 * @property \App\Model\Entity\Publication[] $publications
 */
class EntryType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'label' => true,
        'author' => true,
        'title' => true,
        'journal' => true,
        'year' => true,
        'volume' => true,
        'pages' => true,
        'number' => true,
        'month' => true,
        'eid' => true,
        'note' => true,
        'crossref' => true,
        'keyword' => true,
        'doi' => true,
        'url' => true,
        'file' => true,
        'citeseerurl' => true,
        'pdf' => true,
        'abstract' => true,
        'comment' => true,
        'owner' => true,
        'timestamp' => true,
        'review' => true,
        'search' => true,
        'publisher' => true,
        'editor' => true,
        'series' => true,
        'address' => true,
        'edition' => true,
        'howpublished' => true,
        'lastchecked' => true,
        'booktitle' => true,
        'organization' => true,
        'language' => true,
        'chapter' => true,
        'type' => true,
        'school' => true,
        'nationality' => true,
        'yearfiled' => true,
        'assignee' => true,
        'day' => true,
        'dayfiled' => true,
        'monthfiled' => true,
        'institution' => true,
        'revision' => true,
        'publications' => true
    ];
}
