<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsMaterials Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\MaterialsTable|\Cake\ORM\Association\BelongsTo $Materials
 * @property \App\Model\Table\MaterialColorsTable|\Cake\ORM\Association\BelongsTo $MaterialColors
 * @property \App\Model\Table\MaterialAspectsTable|\Cake\ORM\Association\BelongsTo $MaterialAspects
 *
 * @method \App\Model\Entity\ArtifactsMaterial get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsMaterial findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsMaterialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_materials');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Materials', [
            'foreignKey' => 'material_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MaterialColors', [
            'foreignKey' => 'material_color_id'
        ]);
        $this->belongsTo('MaterialAspects', [
            'foreignKey' => 'material_aspect_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('is_material_uncertain')
            ->allowEmpty('is_material_uncertain');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['material_id'], 'Materials'));
        $rules->add($rules->existsIn(['material_color_id'], 'MaterialColors'));
        $rules->add($rules->existsIn(['material_aspect_id'], 'MaterialAspects'));

        return $rules;
    }
}
