<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MaterialAspects Model
 *
 * @property \App\Model\Table\ArtifactsMaterialsTable|\Cake\ORM\Association\HasMany $ArtifactsMaterials
 *
 * @method \App\Model\Entity\MaterialAspect get($primaryKey, $options = [])
 * @method \App\Model\Entity\MaterialAspect newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MaterialAspect[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialAspect|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaterialAspect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MaterialAspect findOrCreate($search, callable $callback = null, $options = [])
 */
class MaterialAspectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('material_aspects');
        $this->setDisplayField('material_aspect');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'material_aspect_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_materials'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('material_aspect')
            ->maxLength('material_aspect', 45)
            ->allowEmpty('material_aspect');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
