<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ruler $ruler
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($ruler) ?>
            <legend class="capital-heading"><?= __('Add Ruler') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('ruler');
                echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true]);
                echo $this->Form->control('dynasty_id', ['options' => $dynasties, 'empty' => true]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
