<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($period) ?>
            <legend class="capital-heading"><?= __('Add Period') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('period');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>
