<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection[]|\Cake\Collection\CollectionInterface $collections
 */
?>

<h3 class="display-4 pt-3"><?= __('Collections') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr >
            <!-- <th scope="col"><= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('collection') ?></th>
            <th scope="col"><?= $this->Paginator->sort('geo_coordinates') ?></th>
            <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_private') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($collections as $collection): ?>
        <tr align="left">
            <!-- <td><= $this->Number->format($collection->id) ?></td> -->
            <td><a href="/collections/<?=h($collection->id)?>"><?= h($collection->collection) ?></a></td>
            <td><?= h($collection->geo_coordinates) ?></td>
            <td><?= $this->Number->format($collection->slug) ?></td>
            <td><?= h($collection->is_private) ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $collection->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $collection->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $collection->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


