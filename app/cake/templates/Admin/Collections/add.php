<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($collection) ?>
            <legend class="capital-heading"><?= __('Add Collection') ?></legend>
            <?php
                echo $this->Form->control('collection');
                echo $this->Form->control('geo_coordinates');
                echo $this->Form->control('slug');
                echo $this->Form->control('is_private');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
