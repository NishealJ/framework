<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material $material
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($material) ?>
            <legend class="capital-heading"><?= __('Add Material') ?></legend>
            <?php
                echo $this->Form->control('material');
                echo $this->Form->control('parent_id', ['options' => $parentMaterials, 'empty' => true]);
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
