<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<h3 class="display-4 pt-3"><?= __('Users') ?></h3>

<div>
    <div>
    
        <table class="table table-hover" cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead  align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Username') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Created On') ?></th>
                    <th class="actions" scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr align="left">
                        <td> 
                            <?= $this->Html->link(__($user->username), '/admin/users/'.$user->username) ?>
                        </td>
                        <td align="left"><?= h($user->created_at) ?></td>
                        <td class="actions">
            <?= $this->Html->link(__('Edit'), ['action' => 'edit/'.$user->username],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?php if($user->active == 1): ?>
            <?= $this->Form->postLink(__('Deactivate'), ['action' => 'delete',$user->username], ['block'=>true,'confirm' => __('Are you sure you want to deactivate # {0}?',  $user->username),'class'=>"btn btn-danger btn-sm"]) ?>
            <?php else: ?>
            <?= $this->Form->postLink(__('Activate'), ['action' => 'delete',$user->username], ['block'=>true,'confirm' => __('Are you sure you want to activate # {0}?',  $user->username),'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif; ?>   
                          
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?= $this->Form->end() ?>
       <?= $this->fetch('postLink'); ?>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>

