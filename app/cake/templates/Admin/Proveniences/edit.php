<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($provenience) ?>
            <legend class="capital-heading"><?= __('Edit Provenience') ?></legend>
            <?php
                echo $this->Form->control('provenience');
                echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
                echo $this->Form->control('geo_coordinates');
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $provenience->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $provenience->id)]
            )
        ?>
        </div>

    </div>
</div>
