<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
            <legend class="capital-heading"><?= __('Link articles to publications') ?></legend>
            <div class="form-group">
                <label>Select Article</label><br>
                <input list="articles_suggestions" name="article" id="article" style="width:50%;"><br>
                 <p id="count-article-results" style="font-size: 12px;color: #757575;"></p>
                <datalist id="articles_suggestions">
            
                </datalist>             
            </div>
            <div class="form-group">
                <label>Select Publication</label><br>
                <input list="pubs_suggestions" name="pub" id="pubs" style="width:50%;">
                 <p id="count-pub-results" style="font-size: 12px;color: #757575;"></p>
                <datalist id="pubs_suggestions">
    
                </datalist>             
            </div>
            <div class="form-group">
                <button id="view_button" onclick="view_link()" class="btn btn-primary">View</button>
            </div>

            <div style="display:none;" id="success_link" class="alert alert-success" role="alert">
                Has been linked successfully.
            </div>
            <div style="display:none;" id="success_unlink" class="alert alert-success" role="alert">
                Has been un-linked successfully.
            </div>
            <div>
                <table id="link_status">
                <tr>
                    <th>Article</th>
                    <th> Publication</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </table>
            </div>
    </div>

</div>
<script src="/assets/js/journals_dashboard.js"></script>
