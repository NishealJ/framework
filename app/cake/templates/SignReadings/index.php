<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading[]|\Cake\Collection\CollectionInterface $signReadings
 */
?>
<div class="signReadings index content">
    <h3><?= __('Sign Readings') ?></h3>
    <div class="table-responsive table-hover">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th><?= $this->Paginator->sort('sign_reading') ?></th>
                    <th><?= $this->Paginator->sort('sign_name') ?></th>
                    <th><?= $this->Paginator->sort('meaning') ?></th>
                    <?php if ($access_granted): ?>
                        <th scope="col"><?= __('Action') ?></th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($signReadings as $signReading): ?>
                <tr> 
                    <td align="left"><a href="SignReadings/<?=h($signReading->id)?>"><?= h($signReading->sign_reading) ?></a></td>
                    <td align="left"><?= h($signReading->sign_name) ?></td>
                    <td align="left"><?= h($signReading->meaning) ?></td>
                    <td>
                    <?php if ($access_granted): ?>
                        <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $signReading->id],
                            ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $signReading->id], 
                            ['confirm' => __('Are you sure you want to delete # {0}?',  $signReading->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                    <?php endif ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>
