<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

 $current_heading="My";
 $breadcrumb = FALSE;
 echo $breadcrumb;
 $includeBread = TRUE;                                                                          
//  echo $current_heading;
?>


<main class="row justify-content-md-center">

    <div class="col-lg-6 boxed bg-light rounded" >
        <div class="capital-heading text-center"><?= __('My Account') ?></div>

        <form>
        <fieldset disabled>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">Username</label>
                        <input type="text" style="width:100%" id="input-username" readonly class="form-control form-control-alternative bg-white rounded border border-white"  value=<?= h($user['username']) ?>>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label" for="input-email">Email</label>
                        <input type="text" style="width:100%" id="input-username" readonly class="form-control form-control-alternative bg-white rounded border border-white"  value=<?= h($user['email']) ?>>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label" >Created</label>
                        <input type="text" style="width:100%" value="<?= h($user['created_at']) ?>"  readonly class="form-control bg-white rounded border border-white"  >
                    
                    </div>
                </div>
            </div>

        
            <div class="row">
                <div class="col">
                    <div class="form-group">


                        <?php if (in_array(1, $user['roles'])) { ?>
                            <label class="form-control-label" >Administrator</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                        <?php } ?>


                        <?php if (in_array(2, $user['roles'])) { ?>
                            <label class="form-control-label" >Editor</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                        <?php } ?>


                        <?php if (in_array(3, $user['roles'])) { ?>
                            <label class="form-control-label" >Can Download HD Images</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>


                        <?php if (in_array(4, $user['roles'])) { ?>
                            <label class="form-control-label" >Can View Private Artifacts</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>

                        <?php if (in_array(5, $user['roles'])) { ?>
                            <label class="form-control-label" >Can View Private Inscriptions</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                        <?php } ?>

                        <?php if (in_array(6, $user['roles'])) { ?>
                        <label class="form-control-label" >Can Edit Transliterations</label>
                        <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>
                        <?php if (in_array(7, $user['roles'])) { ?>
                            <label class="form-control-label" >Can View Private Images</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>
                        <?php if (in_array(8, $user['roles'])) { ?>
                            <label class="form-control-label" >Can View CDLI Tablet Managers</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>
                        <?php if (in_array(9, $user['roles'])) { ?>
                            <label class="form-control-label" >Can View CDLI Forums</label>
                            <input type="text" style="width:100%" value="Yes"  readonly class="form-control bg-white rounded border border-white"  >
                    
                        <?php } ?>
                    </div>
                </div>
            </div>
            
        </fieldset >
        </br>
            <div class=" row float-right">
            <?= $this->Html->link(__('Change Password'), ['action' => 'profile', 'edit'], ['class' => 'btn btn-outline-secondary mx-1 ']) ?>

            <?php if(TRUE){ ?>
            <?= $this->Html->link(__('Edit Author Profile'), ['action' => ''], ['class' => 'btn btn-secondary']) ?>
            <?php } ?>
            </div>
        </form>
    </div>

</main>
