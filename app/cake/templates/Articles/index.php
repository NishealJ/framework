<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */

$title = [
    'cdlb' => 'Cuneiform Digital Library Bulletins',
    'cdlj' => 'Cuneiform Digital Library Journals',
    'cdln' => 'Cuneiform Digital Library Notes',
    'cdlp' => 'Cuneiform Digital Library Preprints'
][$type];

$edit_type = [
    'cdlb' => 'CDLB',
    'cdlj' => 'CDLJ',
    'cdln' => 'CDLN',
    'cdlp' => 'CDLP'
][$type];

$hasHtml = $type != 'cdlp';
$hasPdf = $type != 'cdln';
?>

<h3 class="display-4 pt-3">
    <?= __($title) ?>
</h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Author(s)</th>
            <th scope="col">Title</th>
            <th scope="col">Date</th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody align="left" class="journals-view-table">
        <?php foreach ($articles as $article):  ?>
            <tr>
                <td width="5%"><?= $article['serial']; ?></td>
                <td width="25%">
                    <?php foreach ($article['authors'] as $author):
                        $end = $author == end($article['authors']) ? '' : '; '; ?>
                        <a href="/authors/<?= $author['id'] ?>"><?=
                            $author['author']
                        ?></a><?= $end ?>
                    <?php endforeach; ?>
                </td>
                <?php if ($hasHtml): ?>
                    <td width="40%"><a href="<?= $article['article_type']; ?>/<?= $article['id']; ?>"><?= $article['title']; ?></a></td>
                <?php endif; ?>
                <?php if (!($hasHtml)): ?>
                <td width="40%">><a href="https://cdli.ucla.edu/files/publications/<?= $article['pdf_link']; ?>"><?= $article['title']; ?></a></td>
                <?php endif; ?>
                <td width="15%"><?= date("Y-m-d", strtotime($article['created'])); ?></td>
                <?php if ($access_granted): ?>
                <td>
                    <a class="btn btn-warning btn-sm" href="/admin/articles/edit/<?= $edit_type; ?>/<?= $article['id']; ?>">Edit</a>
                </td>
                <?php endif ?>           
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
