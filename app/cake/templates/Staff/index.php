<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff[]|\Cake\Collection\CollectionInterface $staff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Staff'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Staff Types'), ['controller' => 'StaffTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Staff Type'), ['controller' => 'StaffTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="staff index large-9 medium-8 columns content">
    <h3><?= __('Staff') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('staff_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
                <?php if ($access_granted): ?>
                    <th scope="col"><?= __('Action') ?></th>
                <?php endif ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($staff as $staff): ?>
            <tr>
                <td><?= $this->Number->format($staff->id) ?></td>
                <td><?= $staff->has('author') ? $this->Html->link($staff->author->id, ['controller' => 'Authors', 'action' => 'view', $staff->author->id]) : '' ?></td>
                <td><?= $staff->has('staff_type') ? $this->Html->link($staff->staff_type->id, ['controller' => 'StaffTypes', 'action' => 'view', $staff->staff_type->id]) : '' ?></td>
                <td><?= h($staff->sequence) ?></td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $staff->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $staff->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $staff->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                <?php endif ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator'); ?>
</div>
